import React, { useState, useEffect } from 'react';
import { Card, Col, Container, Row } from 'react-bootstrap';
import './Orders.css'

export default function Orders() {
  const [orders, setOrders] = useState([]);
  const [totalAmount, setTotalAmount] = useState("");

  useEffect(() => {
    const fetchOrders = async () => {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/users/orders`);
        const data = await response.json();
        setOrders(data);
        setTotalAmount(data.totalAmount);
    };

    fetchOrders();
  }, []);

  return (
      <div>
        <h1>ALL ORDERS</h1>

        <Container>
          <h2>Orders</h2>
          <Row>
            {Object.entries(orders).map(([userId, userOrders]) => (
              <Col key={userId} md={6} lg={4}>
                <Card>
                  <Card.Header>{userOrders.name}</Card.Header>
                  <Card.Body>
                    <Card.Title>Total Orders: {userOrders.orders.length}</Card.Title>
                    <ul>
                      {userOrders.orders.map((order) => (
                       <li key={order.purchasedOn.toString()}>
                          <p>Date: {new Date(order.purchasedOn).toLocaleString()}</p>
                          <p>Total Amount: {order.subTotal} Php</p>
                          <ul>
                            {order.products.map((product) => (
                            <li key={product._id}>{product.productName}</li>))}
                          </ul>
                        </li>
                      ))}
                    </ul>
                  </Card.Body>
                 <Card.Footer>
                    <p>
                      Total Amount: {userOrders.totalAmount} Php
                    </p>
                  </Card.Footer>
                </Card>
             </Col>
            ))}
          </Row>
        </Container>
      </div>
  );
}