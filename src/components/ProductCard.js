import { Card, Button, CardImg, CardGroup } from 'react-bootstrap';
import { useEffect, useState, useContext } from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";
import { Row, Col } from "react-bootstrap";


export default function ProductCard({productProp}) {

	const {_id, name, description, price, isActive, productImage} = productProp

	  const [index, setIndex] = useState(0);
	
	  const handleSelect = (selectedIndex, e) => {
		setIndex(selectedIndex);
	  };

	return (
	<Col key={_id}>
		<CardGroup>
			<Card>
				<CardImg variant="top" src={productImage} />
	    		    <Card.Body>
	    		        <Card.Title>{name}</Card.Title>
	    		        <Card.Subtitle>Description:</Card.Subtitle>
	    		        <Card.Text>{description}</Card.Text>
	    		        <Card.Subtitle>Price:</Card.Subtitle>
	    		        <Card.Text>{price}</Card.Text>
	    		        <Button as={Link} to={`/products/${_id}`}>BUY</Button>
	    		    </Card.Body>
	    		</Card>
		</CardGroup>
	</Col>
	)
}
