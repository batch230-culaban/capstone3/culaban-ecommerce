// Need to Import
import './App.css';
import React, { useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { Route, Routes } from 'react-router-dom';
import { BrowserRouter as Router} from 'react-router-dom';

import { UserProvider } from './UserContext';
import { useState, useEfect, useContext } from 'react';



// Pages
import Home from './pages/Home_Folder/Home';
import Login from './pages/Login_Folder/Login';
import Register from './pages/Register_Folder/Register';
import Error from './pages/Error_Folder/Error';
import ProductView from './pages/ProductsView_Folder/ProductView';
import Logout from './pages/Logout_Folder/Logout';
import Settings from './pages/Settings_Folder/Settings';
import Products from './pages/Products_Folder/Products';

// Components
import AppNavbar from './components/AppNavbar_Folder/AppNavbar';
import Cart from './pages/Cart_Folder/Cart';
import Orders from './pages/Orders_Folder/Orders';
import AdminNavbar from './components/AdminNavbar_Folder/AdminNavbar';
import AdminDashboard from './pages/Admin_Folder/AdminDashboard';
import Users from './pages/Users_Folder/Users';
import Contact from './pages/Contact_Folder/Contact';




// Start of App Code
export default function App() {

  const [user, setUser] = useState({
    email: localStorage.getItem('email'),
    id: localStorage.getItem('id'),
    isAdmin: localStorage.getItem('isAdmin')
  })

  const unsetUser = () => {

    localStorage.clear();
    
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/Users/details`, {
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(res => res.json())
      .then(data => {
          console.log(data);
      
          if(typeof data._id !== undefined){
            setUser({
                email: data.email,
                id: data._id,
                isAdmin: data.isAdmin
            })
        }else{
            setUser({
              email: data.email,
              id: data._id,
              isAdmin: data.isAdmin
              })
        }
     })
  }, [])

  return (

  <UserProvider value = {{user, setUser, unsetUser}}>
    <Router>
      <Container fluid className='mainContainer'>
        {user && user.isAdmin === true ? (
          <AdminNavbar/> ) : (<AppNavbar />
        )}
        
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/admin" element={<AdminDashboard />} />
          
          <Route exact path="/products" element={<Products />} />
          <Route exact path="/products/:productId" element={<ProductView />} />
          <Route exact path="/login" element={<Login />} />
          <Route exact path="/logout" element={<Logout />} />
          <Route exact path="/register" element={<Register />} />
          <Route exact path="/settings" element={<Settings />} />
          <Route exact path="/cart" element={<Cart />} />
          <Route exact path="/contact" element={<Contact />} />
          <Route exact path="/orders" element={<Orders />} />
          <Route exact path="/users" element={<Users />} />
          <Route exact path="*" element={<Error />} /> 

        </Routes>
      </Container>
    </Router>
  </UserProvider>
  );
}